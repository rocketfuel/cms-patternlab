module.exports = function (grunt) {

    "use strict";

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dev: {
                options: {
                    sourceMap: true
                },
                files: {
                    'source/css/style.css': 'source/css/style.scss'
                }
            },
            dist: {
                files: {
                    'source/css/style.css': 'source/css/style.scss'
                }
            }
        },

        autoprefixer: {
            single_file: {
                options: {
                    browsers: ['last 4 version', 'ie 7' , 'ie 8', 'ie 9'],
                    map: true
                },
                src: 'css/style.css'
            }
        },


        concat: {
            dist: {
                options: {
                    stripBanners: true,
                    banner: '/*! <%= pkg.name %> */'
                },
                src: [
                    'jscripts/lib/jquery-1.11.0.js',
                    'jscripts/lib/jquery-migrate.js',
                    'jscripts/lib/jquery.cycle.all.js',
                    'jscripts/site_js.js'
                ],
                dest: 'jscripts/build/<%= pkg.name %>-<%= pkg.version %>.js'
            }

        },

        uglify: {
            dist: {
                src:  'jscripts/build/<%= pkg.name %>-<%= pkg.version %>.js',
                dest: 'jscripts/build/<%= pkg.name %>-<%= pkg.version %>.min.js'
            }
        },

        watch: {
            css: {
                files: ['source/css/**/*.scss'],
                tasks: ['sass:dev','autoprefixer']
            },

            options: {
                spawn: false
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'css/style.css',
                        '**/*.php',
                        'jscripts/**/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    ghostMode: {
                        forms: false
                    }
                }
            }
        },

        webfont: {
            icons: {
                src: 'images/icons/*.svg',
                dest: 'images/build/icon-fonts',
                destCss: 'sass/_icons.scss',
                options: {
                    engine: 'node',
                    ie7: false,
                    'stylesheet': 'scss',
                    'relativeFontPath': '../images/build/icon-fonts'
                }
            }
        },

        fontface: {
            dist: {
                options: {
                    fontDir: 'fonts',
                    template: '@include rf-font-face($font-family: {{font}}, $file: {{font}}, $short-name: {{font}}, $serif: sans);'
                }
            }

        },

        svg2png: {
            all: {
                files: [
                    { cwd: 'images/', src: ['**/*.svg'], dest: 'images/' }
                ]
            }
        }



    });

    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-fontface');
    grunt.loadNpmTasks('grunt-webfont');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-svg2png');

    // Default task(s).
    grunt.registerTask('style', ['sass:dev', 'autoprefixer']);
    grunt.registerTask('default', ['browserSync', 'watch']);

};
